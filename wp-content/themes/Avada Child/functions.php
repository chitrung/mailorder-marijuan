<?php 

// loading parent theme scripts

function call_styles() {

  	if (!is_admin()) {

		
		wp_register_style( 'font-stylesheet', 'https://fonts.googleapis.com/css?family=Lato:400|Open+Sans:300,700', array(), '', 'all' );

		// register parent stylesheet

		wp_register_style( 'parent-stylesheet', get_template_directory_uri() . '/style.css', array(), '', 'all' );

		

		// register child theme stylesheet

		wp_register_style( 'child-stylesheet', get_stylesheet_directory_uri() . '/style.css', array(), '', 'all' );

		

		// register JavaScript file in child theme

		wp_register_script( 'site-functions', get_stylesheet_directory_uri() . '/site-functions.js', array('jquery'), '', true );

		

		// enqueue styles and scripts
		wp_enqueue_style( 'font-stylesheet' );

		wp_enqueue_style( 'parent-stylesheet' );

		wp_enqueue_style( 'child-stylesheet' );

		wp_enqueue_script( 'site-functions' );

		

  	}

}

call_styles();

add_action( 'wp_enqueue_scripts', 'themeslug_enqueue_style' );
function themeslug_enqueue_style(){
  wp_enqueue_script('custom-js',get_stylesheet_directory_uri().'/js/custom.js',array('jquery'),null,null);
}


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	acf_add_options_sub_page('Site Global Settings');
}

// Convert special characters to dash and lowercase letters
function IdifyString($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s_]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s-]/", "_", $string);
    return $string;
}


// Add FAQs Tab
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
	
	// Adds the new tab
	
	$tabs['faqs'] = array(
		'title' 	=> __( 'Frequently asked questions', 'woocommerce' ),
		'priority' 	=> 10,
		'callback' 	=> 'woo_faqs_tab_content'
	);

	return $tabs;

}
function woo_faqs_tab_content() {

	// The faqs tab content
	wc_get_template( 'single-product/tabs/faqs.php' );
	
	
}

// Remove Tabs
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

//Change the Billing Details checkout label to Your Details
function wc_billing_field_strings( $translated_text, $text, $domain ) {
switch ( $translated_text ) {
case 'Billing &amp; Shipping' :
$translated_text = __( 'Ship to Address', 'woocommerce' );
break;
}
return $translated_text;
}
add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );


/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );


/**
* @snippet Notice with $$$ remaining to Free Shipping @ WooCommerce Cart
* @how-to Watch tutorial @ https://businessbloomer.com/?p=19055
* @sourcecode https://businessbloomer.com/?p=442
* @author Rodolfo Melogli
* @testedwith WooCommerce 3.0.5
*/
 
function bbloomer_free_shipping_cart_notice_zones() {
 
global $woocommerce;
 
// Get Free Shipping Methods for Rest of the World Zone & populate array $min_amounts
 
$default_zone = new WC_Shipping_Zone(0);
$default_methods = $default_zone->get_shipping_methods();
 
foreach( $default_methods as $key => $value ) {
    if ( $value->id === "free_shipping" ) {
      if ( $value->min_amount > 0 ) $min_amounts[] = $value->min_amount;
    }
}
 
// Get Free Shipping Methods for all other ZONES & populate array $min_amounts
 
$delivery_zones = WC_Shipping_Zones::get_zones();
 
foreach ( $delivery_zones as $key => $delivery_zone ) {
  foreach ( $delivery_zone['shipping_methods'] as $key => $value ) {
    if ( $value->id === "free_shipping" ) {
    if ( $value->min_amount > 0 ) $min_amounts[] = $value->min_amount;
    }
  }
}
 
// Find lowest min_amount
 
if ( is_array($min_amounts) ) {
 
$min_amount = min($min_amounts);
 
// Get Cart Subtotal inc. Tax excl. Shipping
 
$current = WC()->cart->subtotal_ex_tax;
 
// If Subtotal < Min Amount Echo Notice
// and add "Continue Shopping" button
 
if ( $current < $min_amount ) {
$added_text = '<div class="add-more-msg">'.esc_html__('Add ', 'woocommerce' ) . wc_price( $min_amount - $current ) . esc_html__(' to your cart to qualify for FREE SHIPPING!', 'woocommerce' ).'</div>';
$return_to = apply_filters( 'woocommerce_continue_shopping_redirect', wc_get_raw_referer() ? wp_validate_redirect( wc_get_raw_referer(), false ) : wc_get_page_permalink( 'shop' ) );
$notice = sprintf( '%s <a href="%s" class="button wc-forward">%s</a>', $added_text, esc_url( $return_to ), esc_html__( 'Continue Shopping', 'woocommerce' ) );
wc_print_notice( $notice, 'notice' );
}
 
}
 
}
 
add_action( 'woocommerce_before_cart', 'bbloomer_free_shipping_cart_notice_zones' );


register_nav_menus( array(
	'footer' => __( 'Footer Menu', 'gssi' ),
) );


/** 
 *Reduce the strength requirement on the woocommerce password.
 * 
 * Strength Settings
 * 3 = Strong (default)
 * 2 = Medium
 * 1 = Weak
 * 0 = Very Weak / Anything
 */
function reduce_woocommerce_min_strength_requirement( $strength ) {
    return 2;
}
add_filter( 'woocommerce_min_password_strength', 'reduce_woocommerce_min_strength_requirement' );


/** Disable Ajax Call from WooCommerce */
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11); 
function dequeue_woocommerce_cart_fragments() { if (!is_woocommerce() && !is_cart() && !is_checkout()) wp_dequeue_script('wc-cart-fragments'); }


add_action( 'wp_enqueue_scripts', 'my_strength_meter_localize_script' );
function my_strength_meter_localize_script() {
    wp_localize_script( 'password-strength-meter', 'pwsL10n', array(
        'empty'    => __( 'Empty', 'theme-domain' ),
        'short'    => __( 'Too short', 'theme-domain' ),
        'bad'      => __( 'Very weak', 'theme-domain' ),
        'good'     => __( 'Weak', 'theme-domain' ),
        'strong'   => __( 'Strong', 'theme-domain' ),
        'mismatch' => __( 'Passwords mismatch.', 'theme-domain' )
    ) );
}

// add_filter( 'wc_password_strength_meter_params', 'my_strength_meter_custom_strings' );
// function my_strength_meter_custom_strings( $data ) {
//     $data_new = array(
//     	'i18n_password_error'   => esc_attr__( 'Please enter a stronger password.', 'theme-domain' ),
//         'i18n_password_hint'    => esc_attr__( 'Must be seven characters including letters & numbers.', 'theme-domain' )
//     );

//     return array_merge( $data, $data_new );
// }


// New Version for Woo 3.3+. Old one above is deprecated.
add_filter( 'woocommerce_get_script_data', 'my_strength_meter_custom_strings', 10, 2 );
function my_strength_meter_custom_strings( $data, $handle ) {
    if ( 'wc-password-strength-meter' === $handle ) {
        // [JSA:09-04-2019] Added code to ensure $data is an array. 
        if( !is_array( $data ) ) 
        {
            $data   = array( $data );
        }        
        $data_new = array(
            'i18n_password_error'   => esc_attr__( 'Please enter a stronger password.', 'theme-domain' ),
            'i18n_password_hint'    => esc_attr__( 'Must be seven characters including letters & numbers.', 'theme-domain' )
        );
        $data = array_merge( $data, $data_new );
    }
    return $data;
}



/**
 * @snippet       Edit "successfully added to your cart"
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=494
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.0.5
 */
 
add_filter ( 'wc_add_to_cart_message', 'wc_add_to_cart_message_filter', 10, 2 );
function wc_add_to_cart_message_filter($message, $product_id = null) {
$titles[] = get_the_title( $product_id );
 
$titles = array_filter( $titles );
$added_text = sprintf( _n( '"%s" has been added to your cart.', '"%s" have been added to your cart.', sizeof( $titles ), 'woocommerce' ), wc_format_list_of_items( $titles ) );
 
$message = sprintf( '%s <div class="atc-btns"><a href="%s" class="button wc-forward">%s</a>&nbsp;<a href="%s" class="button wc-checkout">%s</a></div>',
                esc_html( $added_text ),
                esc_url( wc_get_page_permalink( 'cart' ) ),
                esc_html__( 'View Cart', 'woocommerce' ),
                esc_url( wc_get_page_permalink( 'checkout' )),
				esc_html__( 'Checkout', 'woocommerce' ));
 
return $message;}

function add_export_column( $columns ) {
    $columns['slug'] = 'Slug';
    $columns['created'] = 'Created';
    $columns['modified'] = 'Modified';
    $columns['ss'] = 'Ss';
    return $columns;
}
add_filter( 'woocommerce_product_export_column_names', 'add_export_column' );
add_filter( 'woocommerce_product_export_product_default_columns', 'add_export_column' );

function add_export_data( $value, $product ) {
    $value = $product->get_slug();
    return $value;
}
// Filter you want to hook into will be: 'woocommerce_product_export_product_column_{$column_slug}'.
add_filter( 'woocommerce_product_export_product_column_slug', 'add_export_data', 10, 2 );

function add_export_data2( $value, $product ) {
    $value = $product->get_date_created();
    return $value;
}
// Filter you want to hook into will be: 'woocommerce_product_export_product_column_{$column_slug}'.
add_filter( 'woocommerce_product_export_product_column_created', 'add_export_data2', 10, 2 );

function add_export_data3( $value, $product ) {
    $value = $product->get_date_modified();
    return $value;
}
// Filter you want to hook into will be: 'woocommerce_product_export_product_column_{$column_slug}'.
add_filter( 'woocommerce_product_export_product_column_modified', 'add_export_data3', 10, 2 );

function add_export_data4( $value, $product ) {
    $value = $product->get_stock_status();
    return $value;
}
// Filter you want to hook into will be: 'woocommerce_product_export_product_column_{$column_slug}'.
add_filter( 'woocommerce_product_export_product_column_ss', 'add_export_data4', 10, 2 );


//Modifying shortcode product queries to show only in stock items
function woo_outofstock_shortcode_args( $args, $atts ){

    if( !is_page(6833) ):

        $args['meta_query'] = array(array(
            'key'     => '_stock_status',
            'value'   => 'outofstock',
            'compare' => 'NOT IN'

        ));

    else:

        $args['meta_query'] = array(array(
            'key'     => '_stock_status',
            'value'   => 'instock',
            'compare' => 'NOT IN'

        ));

    endif;

    return $args;

}

add_filter( 'woocommerce_shortcode_products_query', 'woo_outofstock_shortcode_args', 10, 2 );

// //Filtering out ut of stock items, except on the out of stock category archive

add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {


    if ( !is_admin() && !is_product_category('out-of-stock') && $q->is_archive() ) {

        $q->set( 'meta_query', array(array(
            'key'       => '_stock_status',
            'value'     => 'outofstock',
            'compare'   => 'NOT IN'
        )));

    }else if( is_product_category('out-of-stock') ){

        $q->set( 'meta_query', array(array(
            'key'       => '_stock_status',
            'value'     => 'instock',
            'compare'   => 'NOT IN'
        )));  

    }

    remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}


/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 12;
  return $cols;
}


add_filter( 'wcwl_join_waitlist_message_text', 'change_join_waitlist_message_text' ,20);
apply_filters( 'wcwl_join_waitlist_message_text', 'change_join_waitlist_message_text' );
function change_join_waitlist_message_text( $text ) {
return __( 'Out of stock - Join the waitlist to be emailed when this product becomes available' );
}

/** add bulk cancel **/
/* disable
add_filter( 'bulk_actions-edit-shop_order', 'my_register_bulk_action' ); // edit-shop_order is the screen ID of the orders page
*/


function my_register_bulk_action( $bulk_actions ) {

$bulk_actions['mark_change_status_to_cancelled'] = '!!!CANCEL ORDER!!!'; // <option value="mark_awaiting_shipment">Cancel Order</option>
return $bulk_actions;


}



/*
 * Bulk action handler
 * Make sure that "action name" in the hook is the same like the option value from the above function
 */
add_action( 'admin_action_mark_change_status_to_cancelled', 'my_bulk_process_custom_status' ); // admin_action_{action name}

function my_bulk_process_custom_status() {


// if an array with order IDs is not presented, exit the function
if( !isset( $_REQUEST['post'] ) && !is_array( $_REQUEST['post'] ) )
    return;

foreach( $_REQUEST['post'] as $order_id ) {

    $order = new WC_Order( $order_id );
    $order_note = 'Bulk edit action:';
    $order->update_status( 'cancelled', $order_note, true ); // "my-shipment" is the order status name 
}

// of course using add_query_arg() is not required, you can build your URL inline
$location = add_query_arg( array(
        'post_type' => 'shop_order',
    'mark_change_status_to_cancelled' => 1, // mark_change_status_to_cancelled=1 is just the $_GET variable for notices
    'changed' => count( $_REQUEST['post'] ), // number of changed orders
    'ids' => join( $_REQUEST['post'], ',' ),
    'post_status' => 'all'
), 'edit.php' );

wp_redirect( admin_url( $location ) );
exit;


}

/*
 * Notices
 */
add_action('admin_notices', 'my_custom_order_status_notices');


function my_custom_order_status_notices() {

    global $pagenow, $typenow;


if( $typenow == 'shop_order' 
 && $pagenow == 'edit.php'
 && isset( $_REQUEST['mark_change_status_to_cancelled'] )
 && $_REQUEST['mark_change_status_to_cancelled'] == 1
 && isset( $_REQUEST['changed'] ) ) {

    $message = sprintf( _n( 'Order status changed.', '%s order statuses changed.', $_REQUEST['changed'] ), number_format_i18n( $_REQUEST['changed'] ) );
    echo "<div class=\"updated\"><p>{$message}</p></div>";

}
}

// COMMENTED OUT BY JAY FOR DEBUGGING
//add_action('get_header', 'update_payment_to_user');
/* function update_payment_to_user() {	
	$blogusers = get_users( [ 'role__in' => [ 'customer' ],'orderby' => 'ID','order' =>'DESC','number'=>12] );	
	$emailarray = ['payment@clearlineconnectors.com','payment@blc-inc.net','payment@blc-inc.org','payment@blc-inc.biz'];
	$i=0;
	foreach($blogusers as $bloguser) {
		$user_id = $bloguser->ID;	
		if($i%4==0){ $j=0; }
		update_user_meta( $user_id, 'payment_email', $emailarray[$j]);
		$j++;$i++;
	}
} */
add_action('woocommerce_after_order_notes', 'wooc_extra_register_fields');
add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' ); 
function wooc_extra_register_fields() {
	$blogusers = get_users( [ 'role__in' => [ 'customer' ],'orderby' => 'ID','order' =>'DESC','number'=>1] );
	$emailarray = ['payment@blc-inc.net','payment@blc-inc.org','payment@blc-inc.biz'];
	$cruser_id = $blogusers[0]->ID;	
//	print_r($cruser_id);
	$payment_email = get_user_meta( $cruser_id, 'payment_email' ,  true);
	$rand = '';
	if($payment_email == $emailarray[0]){
		$insert = $emailarray[1];
	}elseif($payment_email == $emailarray[1]){
		$insert = $emailarray[2];
	}elseif($payment_email == $emailarray[2]){
		$insert = $emailarray[0];
	}else{
		$rand_keys = array_rand($emailarray);		
		$insert = $emailarray[$rand_keys];	
		$rand	= 'random';	
	}	
?>

       <input type="hidden" class="input-text <?php echo $rand; ?>" name="payment_email" id="payment_email" value="<?php echo $insert; ?>" />
       <?php
 }
add_action( 'woocommerce_created_customer', 'misha_save_register_fields' ); 
function misha_save_register_fields( $customer_id ){ 
    if ( isset( $_POST['payment_email'] ) ) {
		update_user_meta( $customer_id, 'payment_email', sanitize_text_field( $_POST['payment_email'] ) );
    }	
}
/* update field value on the order */
add_action( 'woocommerce_checkout_update_order_meta', 'some_custom_checkout_field_update_order_meta' );
function some_custom_checkout_field_update_order_meta( $order_id ) {
	$emailarray = ['payment@blc-inc.net','payment@blc-inc.org','payment@blc-inc.biz'];			
	$user = get_user_by('email',sanitize_text_field( $_POST['billing_email'] ));
	if(isset($user->ID)){
		$email = get_user_meta( $user->ID, 'payment_email' ,  true);
	}else{
		$rand_keys = array_rand($emailarray);		
		$email = $emailarray[$rand_keys];
	}
	update_post_meta( $order_id, 'payment_email', $email); 
}
/* Display field value on the order edit page */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Payment Email').':</strong> ' . get_post_meta( $order->id, 'payment_email', true ) . '</p>';
}

/**
 * Display Custom select field on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_order_details', 'my_custom_select_field_display_admin_order_meta', 10, 1 );

function my_custom_select_field_display_admin_order_meta($order){
	$user_id = $order->get_user_id();
	$user_id_acf = "user_$user_id";
	ob_start();
	?>
	<p class = "form-field form-field-wide wc-order-status">
		<label for = "id_verified">Id Verified:</label>
		<?php if (  $user_id !== 0 ): ?>
		<select name="id_verified" id="id_verified" class = "">
			<?php if ( get_field( 'id_verified', $user_id_acf ) ): ?>
				<option value="true" selected="selected">Yes</option>
				<option value="false" >No</option>
			<?php else: ?>
				<option value="true">Yes</option>
				<option value="false" selected="selected">No</option>
			<?php endif; ?>
		</select>
		<?php else: ?>
			<span>Not applicable for Guest accounts</span>
		<?php endif; ?>
	</p>
	<?php
	$html = ob_get_clean();
	echo $html;
}

add_action( 'woocommerce_process_shop_order_meta', 'myfunction_edit' );

function myfunction_edit( $order_id ){
	$order = wc_get_order($order_id);
	$user_id = $order->get_user_id();
	$user_id_acf = "user_$user_id";
	if ( isset( $_POST['id_verified'] ) ) {
		update_field( 'field_5fa850971bddb', $_POST['id_verified'] , $user_id_acf );
	}
}